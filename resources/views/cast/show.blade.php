@extends('template.master')

@section('head')
    Detail Data Pemain
@endsection

@section('title')
    Detail Data Pemain
@endsection

@section('content')
    <h2>Data Pemain {{$cast->id}}</h2>
    <p>{{$cast->nama}}</p>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection