@extends('template.master');

@section('head')
    Tambah Daftar Nama Pemain
@endsection

@section('title')
    Tambah Daftar Nama Pemain Film Baru 
@endsection

@section('content')
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Pemain</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Pemain">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label><br>
                <input type="number"  class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Pemain">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label><br>
                <textarea name="bio"  class="form-control" id="bio" cols="50" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection