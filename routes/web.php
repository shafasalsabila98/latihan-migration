<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cast/create',[App\Http\Controllers\CastController::class, 'create']);
Route::post('/cast',[App\Http\Controllers\CastController::class, 'store']);
Route::get('/cast',[App\Http\Controllers\CastController::class, 'index']);
Route::get('/cast/{cast_id}',[App\Http\Controllers\CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit',[App\Http\Controllers\CastController::class, 'edit']);
Route::put('/cast/{cast_id}',[App\Http\Controllers\CastController::class, 'update']);
Route::delete('/cast/{cast_id}',[App\Http\Controllers\CastController::class, 'destroy']);